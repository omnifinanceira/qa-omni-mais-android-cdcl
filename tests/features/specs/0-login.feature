#language:pt
@0 @login
Funcionalidade: Login no aplicativo OmniMais

@login-sucesso
Cenario: Quando eu realizo o login com sucesso

Quando eu preencher o campo <login>
    E preencho o campo <senha>
    E clico no botao Entrar
    Entao eu espero visualizar a home do OmniMais
    
Exemplos:
    |login     |senha     |
    |"331luana"|"senha123"|