#language:pt
@6 @refinanciamento-moto
Funcionalidade: Criar nova proposta refinanciamento motos

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta refinanciamento motos
Quando eu clico em Adicionar ficha refinanciamento moto
E seleciono o tipo de operacao refinanciamento para a ficha moto
E seleciono o produto refinanciamento moto
E seleciono o solicitante lojista para a ficha refinanciamento moto
E seleciono a loja refinanciamento moto
E seleciono o vendedor refinanciamento moto
E preencho o <cpf> do cliente refinanciamento moto
E preencho <datanascimento> do cliente refinanciamento moto
E preencho o numero de celular do cliente refinanciamento moto
E preencho a renda do cliente refinanciamento moto
E insiro dados do veiculo <placa> refinanciamento moto
E carrego o resultado parcial refinanciamento moto

Exemplos:
    |cpf          |datanascimento  |placa    |
    |"04333332869"|"06/08/1962"    |"FHC9383"|