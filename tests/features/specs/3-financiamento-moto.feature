#language:pt
@3 @financiamento-moto
Funcionalidade: Criar nova proposta financiamento motos

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta financiamento motos
Quando eu clico em Adicionar ficha financiamento moto
E seleciono o tipo de operacao financiamento para a ficha moto
E seleciono o produto financiamento moto
E seleciono o solicitante lojista para a ficha financiamento moto
E seleciono a loja financiamento moto
E seleciono o vendedor financiamento moto
E preencho o <cpf> do cliente financiamento moto
E preencho <datanascimento> do cliente financiamento moto
E preencho o numero de celular do cliente financiamento moto
E preencho a renda do cliente financiamento moto
E insiro dados do veiculo <placa> financiamento moto
E carrego o resultado parcial financiamento moto

Exemplos:
    |cpf          |datanascimento  |placa    |
    |"04333332869"|"06/08/1962"    |"FHC9383"|