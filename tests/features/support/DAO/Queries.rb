class Queries < SQLConnect
 
    def pegar_cpf
        lista_cpf = select("SELECT a.cpf_cgc
            FROM clientes_proposta  a,
                 retorno_neuro_server b
            WHERE a.proposta = b.proposta
            AND pontuacao > 95
            AND TRUNC(data_execucao) BETWEEN '01-jan-2019'  
            AND Trunc(SYSDATE)")
        return lista_cpf.shuffle.first
    end

    def pegar_placa(veiculo='leve', renavam=false)
        #VEÍCULO LEVE
        #MOTO
        #VEÍCULO PESADO
        @veiculo = veiculo
        if(@veiculo.downcase.include?('leve'))
            @veiculo = 'VEÍCULO LEVE'
        elsif(@veiculo.downcase.include?('moto'))
            @veiculo = 'MOTO'
        else
            @veiculo = 'VEÍCULO PESADO'
        end

        if renavam
            renavam = 'AND gp.cod_renavam is not null'
        end

        placa_veiculo = select("SELECT DISTINCT gp.placa
            FROM garantias_veiculos_proposta gp,
            proposta p,
            operacoes op
            WHERE gp.proposta = p.proposta
            AND p.operacao = op.codigo
            AND op.grupo2 = '#{@veiculo}'
            AND p.emissao >= trunc(SYSDATE - 30) --ULTIMOS 30 DIAS
            AND gp.placa is not null
            AND gp.chassi is not null")

        return placa_veiculo.shuffle.first[0]
    end

    def pegar_dados_proposta_banco(numero_proposta)
        lista_dados = select("SELECT
            TO_CHAR(PROPOSTA),
            TO_CHAR(CLIENTE),
            TO_CHAR(VALOR_COMPRA),
            TO_CHAR(VALOR_ENTRADA),
            TO_CHAR(VALOR_IOC),
            TO_CHAR(VALOR_LIQ),
            TO_CHAR(VALOR_TOT),
            TO_CHAR(VALOR_CON),
            TO_CHAR(PARCELAS),
            TO_CHAR(AGENTE),
            TO_CHAR(LOJISTA),
            TO_CHAR(TABELA),
            TO_CHAR(LOCAL),
            TO_CHAR(STATUS),
            TO_CHAR(USUARIO_PROPOSTA),
            TO_CHAR(FL_MOSTRA),
            TO_CHAR(SCORE),
            TO_CHAR(DATA_INCLUSAO),
            TO_CHAR(SIMULACAO_PROPOSTA),
            TO_CHAR(VALOR_INICIAL),
            TO_CHAR(GRUPO2),
            TO_CHAR(GRUPO1),
            TO_CHAR(CEP_AGENTE_CADASTRO),
            TO_CHAR(VALOR_SEGURO),
            TO_CHAR(VALOR_SIRCOF),
            TO_CHAR(EMPRESA),
            TO_CHAR(VALOR_PRESTACAO_PROPOSTA)
            FROM PROPOSTA WHERE PROPOSTA = #{numero_proposta}")
        return lista_dados.shuffle.first
    end

    def pegar_dados_clientes_proposta(numero_proposta)
        lista_dados = select("SELECT 
            TO_CHAR(PROPOSTA), 
            TO_CHAR(CLIENTE), 
            TO_CHAR(NOME), 
            TO_CHAR(CPF_CGC), 
            TO_CHAR(CATEGORIA), 
            TO_CHAR(OPERADOR)
            FROM CLIENTES_PROPOSTA WHERE PROPOSTA = #{numero_proposta}")
        return lista_dados.shuffle.first
    end

    def pegar_dados_clientes_telefones_proposta(numero_proposta)
        lista_dados = select("SELECT 
            TO_CHAR(CATEGORIA_TELEFONE), 
            TO_CHAR(DDD), 
            TO_CHAR(NR_TELEFONE) 
            FROM CLIENTES_TELEFONES_PROPOSTA WHERE PROPOSTA = #{numero_proposta}")
        return lista_dados
    end

    def pegar_dados_clientes_fisica_proposta(numero_proposta)
        lista_dados = select("SELECT 
            TO_CHAR(TIPO_MORADIA), 
            TO_CHAR(ESTADO_CIVIL), 
            TO_CHAR(NACIONALIDADE), 
            TO_CHAR(DATA_NASCIMENTO), 
            TO_CHAR(EMAIL), 
            TO_CHAR(MAE), 
            TO_CHAR(PAI), 
            TO_CHAR(NATURAL_DE), 
            TO_CHAR(NATURAL_DE_UF)
            FROM CLIENTES_FISICA_PROPOSTA WHERE PROPOSTA = #{numero_proposta}")
        return lista_dados.shuffle.first
    end

    def pegar_dados_clientes_enderecos_proposta(numero_proposta)
        lista_dados = select("SELECT 
            TO_CHAR(TIPO_ENDERECO), 
            TO_CHAR(UF), 
            TO_CHAR(CIDADE), 
            TO_CHAR(ENDERECO), 
            TO_CHAR(NUMERO), 
            TO_CHAR(BAIRRO), 
            TO_CHAR(CEP), 
            TO_CHAR(LOCAL_CORRESPONDENCIA) 
            FROM CLIENTES_ENDERECOS_PROPOSTA WHERE PROPOSTA = #{numero_proposta}")
        return lista_dados
    end

end