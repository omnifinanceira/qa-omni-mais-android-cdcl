require "selenium-webdriver"
require_relative 'helper.rb'
require 'appium_capybara'
require 'appium_lib'
require 'pry'
require 'oci8'
require_relative '../pages/util.rb'
require_relative '../support/DAO/SQLConnect.rb'
require_relative '../support/DAO/Queries.rb'

BANCO = YAML.load_file(File.dirname(__FILE__) + "/DAO/config.yml")

World(Helper)

  util = Util.new
  util.criar_pasta_log

  def caps
    {caps: {
      deviceName: "emulator-5554",
      platformName: "Android",
      noReset: "false",
      fullReset: "false",
      appActivity: "br.com.omni.mais.ui.LauncherActivity",
      automationName: "UIAutomator2",
      appPackage: "br.com.omni.mais.hmg",
      newCommandTimeout: 3600,
    }}
  end
  
  # appPackage: "br.com.omni.mais.debug", -- dev
  # appPackage: "br.com.omni.mais.hmg", -- hmg
Appium::Driver.new(caps, true)
Appium.promote_appium_methods Object