Dado('que eu esteja logado como lojista') do
  login = Login.new
  login.logar
end

Quando('eu clico em Adicionar ficha financiamento pesado') do
  @novaficha = NovaFicha.new
  @novaficha.clicar_nova_ficha
end

Quando('seleciono o tipo de operacao financiamento para a ficha pesado') do
  @novaficha.selecionar_operacao_financiamento
end

Quando('seleciono o produto financiamento pesado') do
  # @novaficha.selecionar_produto_financiamento pesados
  @novaficha.selecionar_produto('PESADOS')
end

Quando('seleciono o solicitante lojista para a ficha financiamento pesado') do
  @novaficha.solicitante("LOJISTA")
end

Quando('seleciono a loja financiamento pesado') do
  @novaficha.selecionar_loja("LUSA VEÍCULOS")
end

Quando('seleciono o vendedor financiamento pesado') do
  @novaficha.selecionar_vendedor
end

Quando('preencho o {string} do cliente financiamento pesado') do |cpf|
  @novaficha.preencher_cpf(cpf)
end

Quando('preencho {string} do cliente financiamento pesado') do |datanascimento|
  @novaficha.preencher_data_nascimento(datanascimento)
end

Quando('preencho o numero de celular do cliente financiamento pesado') do
  @novaficha.preencher_celular
end

Quando('preencho a renda do cliente financiamento pesado') do
  @novaficha.preencher_renda_cliente
end

Quando('seleciono o comprovante de renda financiamento pesado') do
  @novaficha.selecionar_comprovante_renda
end

Quando('informo conjuge financiamento pesado') do
  @novaficha.adicionar_conjuge(false)
end

Quando('informo {string} caminhao cliente financiamento pesado') do |primeiro|
  primeiro == "true" ? primeiro = true : primeiro = false
  @novaficha.primeiro_caminhao_cliente(primeiro)
end

Quando('preencho {int} e informacoes do caminhao proprio financiamento pesado') do |quantidadecaminhao|
  @novaficha.preencher_caminhao_em_nome_cliente(quantidadecaminhao)
end

Quando('insiro dados do veiculo {string} financiamento pesado') do |placa|
  @novaficha.preencher_dados_veiculo(placa)
end

Quando('carrego o resultado parcial financiamento pesado') do
  @novaficha.aguardar_carregamento_resultado_parcial
end

Quando('ajusto o valor financiado e quantidade de parcelas financiamento pesado') do
  binding.pry
end

Quando('insiro mais informacoes do cliente financiamento pesado') do

end

Quando('passo por informacoes dos caminhoes do proprio cliente financiamento pesado') do

end

Quando('insiro endereco do cliente financiamento pesado') do

end

Quando('preencho observacoes financiamento pesado') do

end

Entao('valido se a ficha foi criada e enviada para analise financiamento pesado') do

end

Entao('validar os dados da proposta financiamento pesado') do

end