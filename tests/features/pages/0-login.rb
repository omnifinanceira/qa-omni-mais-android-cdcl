require_relative 'util.rb'

class Login < Util
    # include Appium::Core
    # def acessar_mobile
    #     Wait.until { find_element(id: "br.com.omni.omni:id/etCpf").send_keys($cpf) }
    #     Wait.until { find_element(id: "br.com.omni.omni:id/btnNext").click }
    #     Wait.until { find_element(id: "br.com.omni.omni:id/etPassword") }
    #     Wait.until { find_element(id: "br.com.omni.omni:id/btnToken") }
    # end

    def preencher_login(login = $loginlojista)
        find_element(:id, "act_login_et_login").send_keys(login)
    end

    def preencher_senha(senha = $senhalojista)
        find_element(:id, "act_login_et_password").send_keys(senha)
    end

    def clicar_entrar
        find_element(:id, "act_login_btn_login").click
    end

    def logar
        preencher_login
        preencher_senha
        clicar_entrar
    end
end