require_relative 'util.rb'

class NovaFicha < GeradorRandomico
    include Appium::Core

    def initialize
        @sql = Queries.new
    end

    def clicar_nova_ficha
        Wait.until { find_element(:id, "act_home_fab_new_proposal").click }
    end

    def selecionar_operacao_financiamento
        Wait.until { find_element(:id, "frag_new_record_operation_type_iv_finicing").click }
    end

    def selecionar_operacao_refinanciamento
        Wait.until { find_element(:id, "frag_new_record_operation_type_iv_refinancing").click }
    end

    def selecionar_produto(veiculo)
        #PESADOS - MOTOCICLETAS - AUTOMÓVEIS
        Wait.until { find_element(:xpath, "*//android.widget.TextView[@text='#{veiculo}']").click }
    end

    def solicitante(solicitando)
        if solicitando.downcase == 'lojista'
            Wait.until { find_element(:id, "frg_who_requested_iv_salesman").click }
        else
            Wait.until { find_element(:id, "frg_who_requested_iv_customer").click }
        end
    end

    def selecionar_primeira_opcao(element)
        Appium::TouchAction.new.press(element:find_element(:id, element)).perform
        sleep(5)
        Wait.until { @centerOfElement = find_element(:id, element).location }
        Wait.until { Appium::TouchAction.new.tap(x: @centerOfElement.x+450, y: @centerOfElement.y+220).perform }
    end

    def pressionar_continuar(botao_continuar)
        Wait.until { find_element(:id, "#{botao_continuar}").click }
    end

    def selecionar_loja(loja)
        Wait.until { find_element(:id, "frg_store_atv_store").send_keys(loja) }
        selecionar_primeira_opcao("frg_store_atv_store")
        pressionar_continuar("frg_store_btn_next")
    end

    def selecionar_vendedor(vendedor="NÃO SOU CADASTRADO")
        Wait.until { find_element(:id, "frg_salesman_atv_salesman").send_keys(vendedor) }
        selecionar_primeira_opcao("frg_salesman_atv_salesman")
        pressionar_continuar("frg_salesman_btn_next")
    end

    def preencher_cpf(cpf)
        Wait.until { find_element(:id, "frg_person_cpf_et_cpf").send_keys(cpf) }
        pressionar_continuar("frg_person_cpf_btn_next")
    end

    def preencher_data_nascimento(data)
        Wait.until { find_element(:id, "frg_person_birthday_et_date").send_keys(data) }
        pressionar_continuar("frg_person_birthday_btn_next")
    end

    def preencher_celular(celular = gerar_telefone_celular)
        Wait.until { find_element(:id, "frg_person_cellphone_et_cellphone").send_keys(celular) }
        pressionar_continuar("frg_person_cellphone_btn_next")
    end

    def preencher_renda_cliente(renda = gerar_renda)
        Wait.until { find_element(:id, "frg_person_income_et_income").send_keys(renda) }
        pressionar_continuar("frg_person_income_btn_next")
    end

    def selecionar_comprovante_renda
        Wait.until { find_element(:xpath, "*//android.widget.TextView[@text='Extrato Bancário']").click }
        pressionar_continuar("frg_client_income_proved_btn_next")
    end

    def adicionar_conjuge(conjuge)
        if conjuge
            Wait.until { find_element(:xpath, "*//android.widget.TextView[@text='Sim']").click }
        else
            Wait.until { find_element(:xpath, "*//android.widget.TextView[@text='Não']").click }
        end
        pressionar_continuar("frg_client_spouse_btn_next")
    end

    def primeiro_caminhao_cliente(primeiro_sim_nao)
        if primeiro_sim_nao
            Wait.until { find_element(:xpath, "*//android.widget.TextView[@text='Sim']").click }
        else
            Wait.until { find_element(:xpath, "*//android.widget.TextView[@text='Não']").click }
        end
        pressionar_continuar("frg_truck_first_btn_next")
    end

    def preencher_quantidade_caminhao(quantidade)
        Wait.until { find_element(:id, "frg_financing_vehicle_truck_amount_et_amount").send_keys(quantidade) }
        pressionar_continuar("frg_financing_vehicle_truck_amount_btn_next")
    end

    def preencher_caminhao_em_nome_cliente(quantidade)
        unless quantidade == 0
            preencher_quantidade_caminhao(quantidade)
            for i in 1..quantidade
                if i > 1
                    Wait.until { find_element(:id,"frg_financing_vehicle_truck_amount_btn_truck").click }
                    Wait.until { find_element(:id,"frg_vehicle_info_et_board").send_keys(@sql.pegar_placa('pesado')) }
                    pressionar_continuar("frg_vehicle_info_btn_next")
                else
                    Wait.until { find_element(:id,"frg_vehicle_info_et_board") .send_keys(@sql.pegar_placa('pesado')) }
                    pressionar_continuar("frg_vehicle_info_btn_next")
                end
            end
            pressionar_continuar("frg_financing_vehicle_truck_amount_btn_next")
        end
    end

    def preencher_dados_veiculo(placa)
        Wait.until { find_element(:id, "frg_vehicle_info_et_board").send_keys(placa) }
        Wait.until { find_element(:id, "frg_vehicle_info_et_uf").send_keys("SP") }
        selecionar_primeira_opcao("frg_vehicle_info_et_uf")
        pressionar_continuar("frg_vehicle_info_btn_next")
    end

    def aguardar_carregamento_resultado_parcial
        begin
            while find_element(:id,"frg_loading_iv_hourglass").displayed?
            end
        rescue
        end
    end

    def selecionar_valor_financiamento(percentual = 0.20)
        binding.pry
        
        valorinicial = find_element(:id, "frg_partial_result_financed_value_sub_title").text.gsub('Máximo:R$','').to_f
        valorminimo = valorinicial*percentual
        valorminimo.round(3) <= 2.000 ? valorminimo = 2.000 : valorminimo = valorminimo.round(3)

        find_element(:id, "frg_partial_result_btn_change_financed_value").click
        Wait.until { find_element(:id, "custom_input_bottom_sheet_et_value").send_keys(valorminimo.to_s + '00') }
        Wait.until { find_element(:id, "custom_input_bottom_sheet_bt_confirm").click }
       
        find_element(:xpath, "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.view.View[4]").click
        pressionar_continuar("")
    end


end